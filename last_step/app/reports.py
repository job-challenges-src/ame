"""
Generates vehicle sales reports based on Sqlite database.
"""

import sqlite3
import csv


class ReportGenerator:
    """
    Implement the following 3 functions:
    - sales_by_brand
    - new_customers
    - old_customers
    """

    def __init__(self):
        self.dbname = "vehicle_crm.sqlite"
        self.con = sqlite3.connect(self.dbname)
        self.cur = self.con.cursor()

    def __del__(self):
        self.con.close()

    def sales_by_brand(self, filename):
        """
        Creates a report of vehicle sales broken by vehicle brand.
        Writes the report into a CSV file.

        Report fields:
        - vehicle_brand: vehicle brand
        - n_sales: number of sales within a group
        - total_value: sum of prices of all sales within a group

        Sort by:
        - n_sales descending
        - total_value descending

        :param str filename: Output file name
        
        """
        SQL = """
            SELECT brand_name, count(sale_id), sum(price) from 
            (SELECT * FROM Sales INNER JOIN Vehicle_models, Vehicles, Invoices) group by brand_name
        """            
        self.cur.execute(SQL)
        rows = self.cur.fetchall()
        with open(filename, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(['vehicle_brand', 'n_sales', 'total_value'])
            writer.writerows(rows)

    def new_customers(self, filename):
        """
        Creates a report of customers who purchased a new vehicle after 1st January 2020
        Writes the report into a CSV file.

        Report fields:
        - customer_name: customer name
        - vehicle_brand: brand of the last purchased vehicle
        - vehicle_model: model of the last purchased vehicle
        - vehicle_year: year of the last purchased vehicle
        - sale_dt: sale date

        Sort by:
        - customer_name ascending

        :param str filename: Output file name
        """
        SQL = """
            SELECT customer_name,brand_name,model_name,vehicle_year,sale_dt from 
            (SELECT * FROM Sales INNER JOIN Vehicle_models, Vehicles, Customers, Invoices)
            where sale_dt > '2020-01-01' 
            group by customer_name order by customer_name
        """
        self.cur.execute(SQL)
        rows = self.cur.fetchall()
        with open(filename, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(['customer_name',
                'vehicle_brand',
                'vehicle_model',
                'vehicle_year',
                'sale_dt',])
            writer.writerows(rows)

    def old_customers(self, filename):
        """
        Creates a report of customers who purchased the last vehicle before 1st January 2016
        Writes the report into a CSV file.

        Report fields:
        - customer_name: customer name
        - vehicle_brand: brand of the last purchased vehicle
        - vehicle_model: model of the last purchased vehicle
        - vehicle_year: year of the last purchased vehicle
        - sale_dt: sale date

        Sort by:
        - customer_name ascending

        :param str filename: Output file name
        """

        SQL = """
            SELECT customer_name,brand_name,model_name,vehicle_year,sale_dt from 
            (SELECT * FROM Sales INNER JOIN Vehicle_models, Vehicles, Customers, Invoices)
            where sale_dt < '2016-01-01' group by customer_name order by customer_name
        """
        self.cur.execute(SQL)
        rows = self.cur.fetchall()
        with open(filename, 'w') as file:
            writer = csv.writer(file)
            writer.writerow(['customer_name',
                'vehicle_brand',
                'vehicle_model',
                'vehicle_year',
                'sale_dt',])
            writer.writerows(rows)
        
