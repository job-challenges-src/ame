"""Phone number anonymizer module."""
import re
from .base_anonymizer import BaseAnonymizer


class PhoneNumberAnonymizer(BaseAnonymizer):
    """Phone number anonymizer.

    Sample usages (parametrized):
    >>> PhoneNumberAnonymizer('X').anonymize('+48 666777888')
    '+48 666777XXX'
    >>> PhoneNumberAnonymizer('X', 5).anonymize('+48 666777888')
    '+48 6667XXXXX'
    >>> PhoneNumberAnonymizer('*', 1).anonymize('+48 666777888')
    '+48 66677788*'
    >>> PhoneNumberAnonymizer('x', 9).anonymize('+55 666777000')
    '+55 xxxxxxxxx'
    """

    def __init__(self, replacement, last_digits=3):
        """
        :param str replacement:
        :param int last_digits:
        """
        if not isinstance(replacement, str) or not isinstance(last_digits, int):
            raise ValueError
        self._repl = replacement
        self._digits = last_digits

    def anonymize(self, text: str) -> str:
        """
        :param str text:
        :rtype: str
        """
        if self._digits == 0:
            return text
        
        for phone_number in re.findall(r'\+[\d]{2} [\d]{9}', text):
            replaced_phone_number = f'{phone_number[:self._digits * -1]}{self._repl * self._digits}'
            text = text.replace(phone_number, replaced_phone_number)
        return text
