"""Email anonymizer module."""
import re
from .base_anonymizer import BaseAnonymizer


class EmailAnonymizer(BaseAnonymizer):
    """Email anonymizer.

    Sample usages (parametrized):
    >>> EmailAnonymizer('...').anonymize('aaa@aaa.com')
    '...@aaa.com'
    >>> EmailAnonymizer('***').anonymize('a-a@a.b.c')
    '***@a.b.c'
    >>> EmailAnonymizer('XXX').anonymize('a.d+a@a-a.com')
    'XXX@a-a.com'
    """

    def __init__(self, replacement):
        """
        :param str replacement:
        """
        if not isinstance(replacement, str):
            raise ValueError
        self._replacement = replacement

    def anonymize(self, text: str) -> str:
        """
        :param str text:
        :rtype: str
        """
        
        for item in re.findall(r'(([\w][\w\.\-\_]?)+@[_\w.-]+\.[a-zA-Z]+)', text):
            email, _ = item
            email_anonymized = re.sub(r'(([\w][\w\.\-\_]?)+@)', f'{self._replacement}@', email)
            text = text.replace(email, email_anonymized)
            
        return text
