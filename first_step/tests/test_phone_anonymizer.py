import pytest

from anonymizer import PhoneNumberAnonymizer

TEST_DATA = [
    ("X", 3, "Lorem ipsum", "Lorem ipsum"),
    ("X", 0, "Lorem +48 666666666 d00r", "Lorem +48 666666666 d00r"),
    ("X", 3, "Lorem +48 666666666 d11r", "Lorem +48 666666XXX d11r"),
    (
        "*",
        3,
        "Lorem +48 666666666, +48 777777777 sit 888888888 amet",
        "Lorem +48 666666***, +48 777777*** sit 888888888 amet",
    ),
]


@pytest.mark.parametrize("replacement,last_digits,text,replaced", TEST_DATA)
def test_anonymize_text(replacement, last_digits, text, replaced):
    anonymizer = PhoneNumberAnonymizer(replacement, last_digits)
    assert anonymizer.anonymize(text) == replaced
