import pytest

from anonymizer import (
    BaseAnonymizer,
    EmailAnonymizer,
    OfferAnonymizer,
    PhoneNumberAnonymizer,
    SkypeUsernameAnonymizer,
)


TEST_DATA = [
    (
        (),
        'Lorem ipsum a@a.com. <a href="skype:loremipsum?call">call</a> +48 666777888',
        'Lorem ipsum a@a.com. <a href="skype:loremipsum?call">call</a> +48 666777888',
    ),
    (
        (
            EmailAnonymizer("REPLACED"),
            SkypeUsernameAnonymizer("REPLACED"),
            PhoneNumberAnonymizer("X", 2),
        ),
        'Lorem ipsum a@a.com. <a href="skype:loremipsum?call">call</a> +48 666777888',
        'Lorem ipsum REPLACED@a.com. <a href="skype:REPLACED?call">call</a> +48 6667778XX',
    ),
]


@pytest.mark.parametrize("anonymizers,text,replaced", TEST_DATA)
def test_anonymize_text(anonymizers, text, replaced):
    offer_anonymizer = OfferAnonymizer()
    for anonymizer in anonymizers:
        offer_anonymizer.add_anonymizer(anonymizer)
    assert offer_anonymizer.anonymize(text) == replaced


def test_class():
    assert issubclass(OfferAnonymizer, BaseAnonymizer)
