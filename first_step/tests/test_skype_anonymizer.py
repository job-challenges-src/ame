import pytest

from anonymizer import SkypeUsernameAnonymizer

TEST_DATA = [
    ("Lorem ipsum", "Lorem ipsum"),
    (
        'Lorem ipsum <a href="skype:loremipsum?call">call</a> dolor sit amet',
        'Lorem ipsum <a href="skype:#?call">call</a> dolor sit amet',
    ),
    (
        'Lorem ipsum  <a href="skype:loremipsum?call">call</a>, dolor sit <a href="skype:IPSUMLOREM?chat">chat</a> amet',
        'Lorem ipsum  <a href="skype:#?call">call</a>, dolor sit <a href="skype:#?chat">chat</a> amet',
    ),
]


@pytest.mark.parametrize("text,replaced", TEST_DATA)
def test_anonymize_text(text, replaced):
    anonymizer = SkypeUsernameAnonymizer("#")
    assert anonymizer.anonymize(text) == replaced
